export { Game, GameTeams, GameState } from './game';
export { Player } from './definitions/player';

/*
const player1 = new Player('player1');
const player2 = new Player('player2');
const player3 = new Player('player3');
const player4 = new Player('player4');

const game = new Game();
// console.log(game);
game.addPlayers(player1, player2, player3, player4);

// console.log(game);
game.doReady();
// console.log(game);
game.start();
// console.log(game);
game.goal(GameTeams.BLUE);
console.log(game);

setTimeout(() => {
  console.log('left', game.timer.timeLeft);
  game.pause();
  game.play();
  setTimeout(() => {
    console.log('left', game.timer.timeLeft);
    game.pause();
  }, 1000);
}, 1000);
*/
