import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

import { Playground } from './definitions/playboard';
import { Player } from './definitions/player';
import { Position } from './definitions/position';
import { Team } from './definitions/team';
import { Coordinate } from './definitions/coordinate';
import { Timer, TimerEvent, TimerEventStatus } from './definitions/timer';
import { Scores } from './definitions/scores';

/** Kinda FSM */
export enum GameState {
  /** Constructor executed */
  INITED,
  /** New game created */
  CREATED,
  /** Players randomized and have teams */
  READY,
  /** Times is on */
  IN_PROGRESS,
  /** Change positions by timer */
  TIMER_POSITION_CHANGE,
  /** Timer on pause */
  PAUSE,
  /** Game over */
  END
}

export enum GameTeams {
  BLUE = 'blue',
  RED = 'red'
}

export const TIME_INTERVAL = 5 * 60 * 1000;

export class Game {
  timer: Timer;
  playground: Playground;

  teams = [
    new Team(GameTeams.BLUE, GameTeams.BLUE),
    new Team(GameTeams.RED, GameTeams.RED)
  ];

  scoresTable: Array<Scores>;
  stateHistory: Array<GameState> = [];
  state = new BehaviorSubject<GameState>(GameState.INITED);

  constructor(
    players: Array<Player> = []
  ) {
    this.scoresTable = this.teams.map(team => new Scores(10, team));
    const positions: Array<Position> = [
      new Position(Coordinate.from(0, 0), this.getTeamByName(GameTeams.RED)),
      new Position(Coordinate.from(1, 0), this.getTeamByName(GameTeams.RED)),
      new Position(Coordinate.from(0, 1), this.getTeamByName(GameTeams.BLUE)),
      new Position(Coordinate.from(1, 1), this.getTeamByName(GameTeams.BLUE))
    ];

    this.playground = new Playground(players, positions);
    this.timer = new Timer(TIME_INTERVAL);

    this.timer.subscribe(event => this.onTimerEvent(event));
    this.state.subscribe(state => this.onChangeState(state));

    this.nextFlowState();
  }

  addPlayers(...players: Array<Player>): void {
    players.forEach(player => this.playground.addPlayer(player));
  }

  goal(forTeam: GameTeams): void {
    if (this.state.value !== GameState.IN_PROGRESS) throw new Error('Game not running');

    const team = this.getTeamByName(forTeam);
    const scores = this.scoresTable.find(item => item.team === team);

    scores.increment();
  }

  doReady(): void {
    this.state.next(GameState.READY);
  }

  start(): void {
    this.state.next(GameState.IN_PROGRESS);
  }

  pause(): void {
    if (this.state.value !== GameState.IN_PROGRESS) throw new Error('Can not be paused');
    this.state.next(GameState.PAUSE);
  }

  play(): void {
    if (
      this.state.value !== GameState.PAUSE &&
      this.state.value !== GameState.TIMER_POSITION_CHANGE
    ) throw new Error('Can not be played');
    this.state.next(GameState.IN_PROGRESS);
  }

  getTeamByName(name: GameTeams): Team {
    return this.teams.find(team => team.id === name);
  }

  // FSM
  private nextFlowState(): void {
    let newState: GameState;

    switch (this.state.value) {
      case GameState.INITED:
        newState = GameState.CREATED;
        break;

      case GameState.CREATED:
        newState = GameState.READY;
        break;

      default:
        throw new Error('no next state');
    }

    this.state.next(newState);
  }

  private onChangeState(toState: GameState): void {
    const previousState = this.stateHistory[this.stateHistory.length - 1];
    this.stateHistory.push(toState);

    switch (toState) {
      case GameState.CREATED:
        console.log('[Game] created');
        break;

      case GameState.READY:
        this.playground.randomizePositions();
        break;

      case GameState.IN_PROGRESS:
        if (previousState === GameState.READY) {
          // First start game
          console.log('[Game] start game');
        }

        this.timer.start();

        break;

      case GameState.PAUSE:
        this.timer.pause();
        break;

      default:
        break;
    }
  }

  private onTimerEvent(event: TimerEvent): void {
    switch (event.status) {
      case TimerEventStatus.TimerDone:
        console.log('[Timer] Done');
        if (this.state.value === GameState.IN_PROGRESS) {
          this.state.next(GameState.TIMER_POSITION_CHANGE);
          this.timer.reset();
        }
        break;

      case TimerEventStatus.TimerPause:
        console.log('[Timer] Pause');
        break;

      case TimerEventStatus.TimerStart:
        console.log('[Timer] Start');
        break;

      default:
        break;
    }
  }
}
