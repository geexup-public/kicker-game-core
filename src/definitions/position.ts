import { Coordinate } from './coordinate';
import { Team } from './team';

/** Position which player can take */
export class Position {
  constructor(
    public coordinate: Coordinate,
    public team: Team
  ) {}
}
