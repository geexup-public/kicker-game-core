import { Player } from './player';
import { Position } from './position';
import { PlaygroundPlayerPosition } from './playground-player-position';

/** Playboard with players on positions */
export class Playground {
  players: Array<Player>;
  positions: Array<Position>;
  playersPositions: Array<PlaygroundPlayerPosition> = [];

  constructor(
    _players: Array<Player> = [],
    _positions: Array<Position> = []
  ) {
    this.players = _players;
    this.positions = _positions;
  }

  addPlayer(player: Player): void {
    this.players.push(player);
  }

  randomizePositions(): void {
    const leftPlayers = [...this.players];

    this.playersPositions = this.positions.map(position => {
      const playerIndex = this.getRandomInt(leftPlayers.length);
      const [player] = leftPlayers.splice(playerIndex, 1);

      return new PlaygroundPlayerPosition(player, position);
    });
  }

  // Random [min, max)
  private getRandomInt(max: number, min = 0): number {
    min = Math.ceil(min); // from (include)
    max = Math.floor(max); // to (not include)

    return Math.floor(Math.random() * (max - min)) + min;
  }
}
