export class Coordinate {
  constructor(
    public x: number,
    public y: number
  ) {}

  static from(x: number, y: number): Coordinate {
    return new Coordinate(x, y);
  }
}
