import { Team } from './team';

export class Scores {
  score = 0;

  constructor(
    public max: number,
    public team: Team
  ) {}

  increment(by = 1): void {
    this.score += by;
  }
}
