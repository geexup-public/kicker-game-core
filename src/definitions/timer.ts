import { Subject } from 'rxjs/internal/Subject';
import { Observer, PartialObserver } from 'rxjs/internal/types';
import { Subscription } from 'rxjs/internal/Subscription';

export enum TimerEventStatus {
  TimerStart,
  TimerPause,
  TimerDone
}

export class TimerEvent {
  constructor(
    readonly status: TimerEventStatus
  ) {}
}

export class Timer {
  private lastInterval: number;
  private lastDate: Date = null;
  private timerId: any = null;
  private state: TimerEventStatus = TimerEventStatus.TimerPause;
  private events = new Subject<TimerEvent>();

  constructor(
    /** Time in ms */
    readonly interval: number
  ) {
    this.reset();
  }

  get timeLeft(): number {
    if (this.state !== TimerEventStatus.TimerStart) {
      if (this.lastInterval) return this.lastInterval;
      if (!this.lastDate) return this.interval;
    }

    return (this.lastInterval ?? this.interval) - ((new Date()).getTime() - this.lastDate.getTime());
  }

  start(): void {
    /** Calculate left time */
    const intervalMs = this.lastInterval ?? this.interval;

    this.events.next(new TimerEvent(TimerEventStatus.TimerStart));
    this.state = TimerEventStatus.TimerStart;
    this.lastDate = new Date();

    this.timerId = setTimeout(() => this.done(), intervalMs);
  }

  reset(): void {
    this.lastDate = null;
    this.lastInterval = null;
    if (this.timerId !== null) clearTimeout(this.timerId);
  }

  pause(): void {
    clearTimeout(this.timerId);
    this.lastInterval = this.timeLeft;
    this.lastDate = new Date();
    this.state = TimerEventStatus.TimerPause;
    this.events.next(new TimerEvent(TimerEventStatus.TimerPause));
  }

  private done(): void {
    this.state = TimerEventStatus.TimerDone;
    this.events.next(new TimerEvent(TimerEventStatus.TimerDone));
    this.reset();
  }

  subscribe(observer: (item: TimerEvent) => void): Subscription {
    return this.events.subscribe(observer);
  }
}
