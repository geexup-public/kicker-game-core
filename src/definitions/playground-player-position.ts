import { Player } from './player';
import { Position } from './position';

export class PlaygroundPlayerPosition {
  constructor(
    public player: Player,
    public position: Position
  ) {}
}
